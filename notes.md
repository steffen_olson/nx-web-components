# Web Components

---

## The Good

- Web Standard - a stable base to build on
- Framework Agnostic - Angular, React, Svelte, or even server generated HTML

# The Bad

- Framework Integration - Supported by pretty much everything, but integration isn't as seemless as building components in the applications framework. React in particular is messy to work with.
- Accessibilty - Custom elements have no semantic meaning by default. This can be overcome by adding roles, but not all native web element semantics are represented by aria roles. https://github.com/webcomponents/gold-standard/wiki/Web-Component-Limitations
