import { Component, EventEmitter, Output } from '@angular/core';
import { of } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';

import { CountChangeEvent, SearchTermChangeEvent } from '@wc-nx/shared/ui';

export declare interface TemporaryComponent {
  something: EventEmitter<CustomEvent<'something'>>;
}

@Component({
  selector: 'wc-nx-temp',
  template: ` <button (click)="emitSomething()">click me!</button> `,
})
export class TemporaryComponent {
  @Output() something = new EventEmitter<CustomEvent<'something'>>();

  emitSomething() {
    this.something.emit(new CustomEvent('something', { detail: 'something' }));
  }
}
@Component({
  selector: 'wc-nx-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  name = 'Steffen';
  count = 3;

  listItems = [1, 'two', new Three()];

  srlListOpen = false;

  searchTerm = '';

  options = [
    { value: 1, label: 'One' },
    { value: 2, label: 'Two' },
    { value: 3, label: 'Three' },
    { value: 4, label: 'Four' },
  ];

  filteredOptions = Array.from(this.options);
  showDialog = false;
  dialogInputValue = '';

  updateCount(ev: CountChangeEvent): void {
    this.count = ev.count;
  }

  srlListToggle(ev: CustomEvent<boolean>) {
    console.log(ev);
    console.log('hello??');
    this.srlListOpen = ev.detail;
  }

  handleSomething(ev: CustomEvent<'something'>) {
    console.log(ev);
  }

  handleSearchTermChange(ev: SearchTermChangeEvent): void {
    console.log(ev);
    this.searchTerm = ev.value;
  }

  filter(ev: Event) {
    const value = (ev as CustomEvent<string>).detail;

    if (value) {
      this.filteredOptions = this.options.filter((opt) =>
        opt.label.includes(value)
      );
    } else {
      this.filteredOptions = Array.from(this.options);
    }
  }
}

class Three {
  toString() {
    return 'three';
  }
}
