import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent, TemporaryComponent } from './app.component';
import { SharedStencilUiNgModule } from '@wc-nx/shared/stencil-ui-ng';
import '@wc-nx/shared/ui';
import '@wc-nx/shared/fast-ui';
import {
  provideFrlDesignSystem,
  frlButton,
  frlDialog,
  frlTextField,
} from '@wc-nx/shared/fast-ui';

provideFrlDesignSystem().register(frlButton(), frlDialog(), frlTextField());

@NgModule({
  declarations: [AppComponent, TemporaryComponent],
  imports: [BrowserModule, SharedStencilUiNgModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule {}
