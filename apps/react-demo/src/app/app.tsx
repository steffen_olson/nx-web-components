import styles from './app.module.css';
import NxWelcome from './nx-welcome';

import {
  CountChangeEvent,
  MyElement as MyElementWc,
  MyElementEventMap,
} from '@wc-nx/shared/ui';
import {
  ReactComponentElement,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';

declare global {
  namespace JSX {
    interface IntrinsicElements {
      ['my-element']: any;
      ['rl-list']: any;
    }
  }
}

interface MyElementProps {
  count?: number;
  onCountChange: (count: number) => void;
}

function MyElement({ count, onCountChange }: MyElementProps) {
  const myElementCallback = useCallback((el: MyElementWc) => {
    if (!el) return;

    const listener = (ev: CountChangeEvent): void => {
      console.log(ev);
      onCountChange(ev.count);
    };

    el.addEventListener('count-change', listener);
  }, []);

  return <my-element count={count} ref={myElementCallback}></my-element>;
}

interface RlListProps {
  items: unknown[];
}

function RlList({ items }: RlListProps) {
  const rlListRefCallback = useCallback((el: any) => {
    if (!el) return;
    el.items = items;
  }, []);

  return <rl-list ref={rlListRefCallback}></rl-list>;
}

class Three {
  toString(): string {
    return 'three';
  }
}

export function App() {
  const [count, setCount] = useState(29);

  const [listItems, setListItems] = useState([1, 'two', new Three()]);

  const changeItems = () => {
    setListItems(['it', 'has', 'changed']);
  };

  return (
    <>
      <MyElement count={count} onCountChange={setCount}></MyElement>
      <RlList items={listItems}></RlList>
      <button onClick={changeItems}>click this</button>
      <div>{count}</div>
    </>
  );
}

export default App;
