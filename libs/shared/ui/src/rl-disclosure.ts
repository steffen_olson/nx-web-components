import {html, LitElement, TemplateResult} from 'lit';
import {customElement, state} from 'lit/decorators.js';

@customElement('rl-disclosure')
export class RlDisclosure extends LitElement {
  @state() expanded = false; // TODO: make property

  toggle(): boolean {
    this.expanded = !this.expanded;
    return this.expanded;
  }

  override render(): TemplateResult {
    return html`
      <button
        aria-expanded=${this.expanded ? 'true' : 'false'}
        @click=${() => this.toggle()}
      >
        toggle
      </button>
      ${this.maybeRenderContent()}
    `;
  }

  private maybeRenderContent(): TemplateResult | string {
    if (!this.expanded) return '';

    return html`
      <div class="disclosure-container">
        <slot></slot>
      </div>
    `;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'rl-disclsure': RlDisclosure;
  }
}
