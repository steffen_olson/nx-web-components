import {LitElement, html, css} from 'lit';
import {customElement, property, query} from 'lit/decorators.js';

export class SearchTermChangeEvent extends Event {
  constructor(public value: string) {
    super('searchTermChange');
  }
}

export interface RlSearchEventMap extends HTMLElementEventMap {
  ['searchTermChange']: SearchTermChangeEvent;
}

export interface RlSearch {
  addEventListener<K extends keyof RlSearchEventMap>(
    type: K,
    listener: (this: RlSearch, ev: RlSearchEventMap[K]) => any,
    options?: boolean | AddEventListenerOptions
  ): void;
  addEventListener(
    type: string,
    listener: EventListenerOrEventListenerObject,
    options?: boolean | AddEventListenerOptions
  ): void;

  removeEventListener<K extends keyof RlSearchEventMap>(
    type: K,
    listener: (this: RlSearch, ev: RlSearchEventMap[K]) => any,
    options?: boolean | AddEventListenerOptions
  ): void;
  removeEventListener(
    type: string,
    listener: EventListenerOrEventListenerObject,
    options?: boolean | EventListenerOptions
  ): void;
}

@customElement('rl-search')
export class RlSearch extends LitElement {
  static override styles = css`
    :host {
      display: inline-flex;
      border-radius: 8px;
      background: transparent;
      transition: border-radius 150ms ease-in-out, background 150ms ease-in-out;
    }

    :host(:not(:focus):hover) {
      background: lightgray;
    }

    :host([hidden]) {
      display: none;
    }

    :host([expanded]) {
      background: lightgray;
      border-radius: 4px;
    }

    :host([expanded]) #searchbox-input {
      width: 250px;
    }

    :host(:focus) {
      box-shadow: 0 0 2px 2px rgba(0, 0, 0, 0.02);
    }

    #searchbox-button {
      border: 0;
      background: 0;
    }

    #searchbox-input {
      width: 0;
      background: transparent;
      color: black;
      font: inherit;
      border: 0;
      transition: width 150ms ease-in-out;
    }

    #searchbox-input:focus,
    #searchbox-input:active {
      outline: 0;
    }
  `;

  @query('#searchbox-input', true) searchInput!: HTMLInputElement;

  @property({type: Boolean, reflect: true}) expanded = false;
  @property({type: String, reflect: true}) searchTerm = '';

  override focus(): void {
    this.expand();
    this.focusInput();
  }

  override blur(): void {
    this.collapse();
    this.blurInput();
  }

  override render() {
    return html`
      <button
        id="searchbox-button"
        tabindex="-1"
        @click=${this.handleSearchButtonClick}
      >
        search
      </button>
      <input
        id="searchbox-input"
        type="search"
        @focus=${this.handleInputFocus}
        @blur=${this.handleInputBlur}
        @input=${this.updateSearchTerm}
        .value=${this.searchTerm}
      />
    `;
  }

  collapse(): void {
    // Don't collapse if the user has entered text.
    if (this.expanded && !this.searchTerm) this.expanded = false;

    // this.blurInput();
  }

  expand(): void {
    if (!this.expanded) this.expanded = true;
    // this.setAttribute('tabindex', '-1');
    // this.focusInput();
  }

  private focusInput(): void {
    this.searchInput.focus();
  }

  private blurInput(): void {
    this.searchInput.blur();
  }

  private updateSearchTerm(ev: InputEvent): void {
    ev.stopPropagation();
    this.searchTerm = this.searchInput.value;
    this.dispatchEvent(new SearchTermChangeEvent(this.searchTerm));
  }

  private handleSearchButtonClick(): void {
    this.focusInput();
  }

  private handleInputBlur(ev: FocusEvent): void {
    // If the input is blurred because the user clicked somewhere within RlSearch, prevent the blur from actually happening.
    if (
      ev.relatedTarget &&
      ev.relatedTarget instanceof Node &&
      this.shadowRoot?.contains(ev.relatedTarget)
    ) {
      ev.preventDefault();
      return;
    }

    this.collapse();
  }

  private handleInputFocus(_ev: FocusEvent): void {
    this.expand();
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'rl-search': RlSearch;
  }
}
