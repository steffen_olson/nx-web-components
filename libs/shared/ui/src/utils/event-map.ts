export interface HasEventMap<TElement, TEventMap> {
  addEventListener<K extends keyof TEventMap>(
    type: K,
    listener: (this: TElement, ev: TEventMap[K]) => any,
    options?: boolean | AddEventListenerOptions
  ): void;
  addEventListener(
    type: string,
    listener: EventListenerOrEventListenerObject,
    options?: boolean | AddEventListenerOptions
  ): void;

  removeEventListener<K extends keyof TEventMap>(
    type: K,
    listener: (this: TElement, ev: TEventMap[K]) => any,
    options?: boolean | AddEventListenerOptions
  ): void;
  removeEventListener(
    type: string,
    listener: EventListenerOrEventListenerObject,
    options?: boolean | EventListenerOptions
  ): void;
}
