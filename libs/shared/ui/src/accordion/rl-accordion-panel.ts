import {LitElement, html, css} from 'lit';
import {customElement, eventOptions, property, query} from 'lit/decorators.js';

export class AccordionToggleEvent extends Event {
  override target!: RlAccordionPanel;

  constructor() {
    super('accordionToggle', {bubbles: true});
  }
}

@customElement('rl-accordion-panel')
export class RlAccordionPanel extends LitElement {
  static override styles = css`
    :host {
      display: block;
    }

    :host([hidden]) {
      display: none;
    }

    :host(:first-child) .accordion-toggle-button {
      border-top-left-radius: 4px;
      border-top-right-radius: 4px;
    }

    :host(:last-child) .accordion-toggle-button[aria-expanded='false'] {
      border-bottom: 2px solid lightgray;
      border-bottom-left-radius: 4px;
      border-bottom-right-radius: 4px;
    }

    :host(:last-child) .accordion-panel-content {
      border-bottom-left-radius: 4px;
      border-bottom-right-radius: 4px;
      border-bottom: 2px solid lightgray;
    }

    .accordion-heading {
      padding: 0;
      margin: 0;
    }

    .accordion-toggle-button {
      display: flex;
      justify-content: space-between;
      width: 100%;
      background: none;
      border: 2px solid lightgray;
      border-bottom: 0;
      padding: 8px;
      margin: 0;
      font-family: inherit;
      font-size: 1rem;
    }

    .accordion-toggle-button:focus {
      outline: 0;
      color: blue;
    }

    .accordion-toggle-label {
      border-bottom: 2px solid transparent;
    }

    .accordion-toggle-button:focus .accordion-toggle-label {
      border-color: blue;
    }

    .accordion-panel-content {
      display: block;
      border: 2px solid lightgray;
      border-bottom: 0;
      padding: 8px;
      background: rgba(0, 0, 0, 0.05);
    }

    .accordion-panel-content[hidden] {
      display: none;
    }
  `;
  @property({reflect: true}) label = '';
  @property({type: Boolean, reflect: true}) expanded = false;

  @query('#accordion-toggle', true) toggleButton!: HTMLButtonElement;

  get toggleFocused(): boolean {
    return this.toggleButton === this.shadowRoot?.activeElement;
  }

  override focus(): void {
    this.toggleButton.focus();
  }

  override blur(): void {
    this.toggleButton.blur();
  }

  toggle(): void {
    this.expanded = !this.expanded;
  }

  collapse(): void {
    this.expanded = false;
  }

  expand(): void {
    this.expanded = true;
  }

  override render() {
    const indicator = this.renderIndicator();

    // TODO: does ID need to be unique? even in a shadow dom?
    // TODO: handle case where button needs to be disabled if it _cannot_ collapse (part of an accordion)
    return html`
      <h3 class="accordion-heading">
        <button
          id="accordion-toggle"
          class="accordion-toggle-button"
          aria-expanded=${this.expanded}
          aria-controls="accordion-panel-content"
          @click=${this.handleToggleClick}
        >
          <span class="accordion-toggle-label">${this.label}</span>
          <span>${indicator}</span>
        </button>
      </h3>
      <div
        id="accordion-panel-content"
        ?hidden=${!this.expanded}
        role="region"
        arria-labelledby="accordion-toggle"
        class="accordion-panel-content"
      >
        <slot></slot>
      </div>
    `;
  }

  private renderIndicator() {
    return this.expanded ? html`&uarr;` : html`&darr;`;
  }

  @eventOptions({passive: true})
  private handleToggleClick(): void {
    this.toggle();
    console.log('dispatching');
    this.dispatchEvent(new AccordionToggleEvent());
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'rl-accordion-panel': RlAccordionPanel;
  }
}
