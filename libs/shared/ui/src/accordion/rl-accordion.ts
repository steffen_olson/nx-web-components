import {LitElement, html, css} from 'lit';
import {
  customElement,
  property,
  queryAssignedElements,
} from 'lit/decorators.js';
import {AccordionToggleEvent, RlAccordionPanel} from '.';

@customElement('rl-accordion')
export class RlAccordion extends LitElement {
  static override styles = css`
    :host {
      display: flex;
      flex-direction: column;
    }

    :host[hidden] {
      display: none;
    }
  `;

  /** Allow multiple simultaneously expanded items */
  @property({type: Boolean, reflect: true}) multi: boolean = false;

  @queryAssignedElements({selector: 'rl-accordion-panel'})
  panels!: Array<RlAccordionPanel>;

  constructor() {
    super();

    this.addEventListener('accordionToggle', (ev) =>
      this.handleAccordionToggle(ev as AccordionToggleEvent)
    );
    this.addEventListener('keydown', (ev: KeyboardEvent) =>
      this.handleKeyDown(ev)
    );
  }

  override render() {
    return html`<slot @slotchange=${this.handleSlotchange}></slot>`;
  }

  private handleSlotchange(): void {
    if (!this.multi) {
      this.synchronize();
    }
  }

  private handleAccordionToggle(ev: AccordionToggleEvent): void {
    if (!this.multi) {
      this.collapseOthers(ev.target);
    }
  }

  private handleKeyDown(ev: KeyboardEvent): void {
    switch (ev.key) {
      case 'ArrowUp':
        this.goToPrevious();
        break;
      case 'ArrowDown':
        this.goToNext();
        break;
      case 'Home':
        this.goToBeginning();
        break;
      case 'End':
        this.goToEnd();
        break;
      default:
        return;
    }

    ev.preventDefault();
  }

  private getFocusedPanelIndex(): number {
    for (let i = 0; i < this.panels.length; i++) {
      if (this.panels[i].toggleFocused) {
        return i;
      }
    }

    return -1;
  }

  private goToPrevious(): void {
    const focused = this.getFocusedPanelIndex();
    if (focused >= 0) this.goTo(focused - 1);
  }

  private goToNext(): void {
    const focused = this.getFocusedPanelIndex();
    if (focused >= 0) this.goTo(focused + 1);
  }

  private goToBeginning(): void {
    const focused = this.getFocusedPanelIndex();
    if (focused >= 0) this.goTo(0);
  }

  private goToEnd(): void {
    const focused = this.getFocusedPanelIndex();
    if (focused >= 0) this.goTo(this.panels.length - 1);
  }

  private goTo(index: number) {
    this.panels[index].focus();
  }

  private synchronize() {
    let anyOpen = false;
    for (const panel of this.panels) {
      if (anyOpen) {
        panel.collapse();
      } else if (panel.expanded) {
        anyOpen = true;
      }
    }
    if (!anyOpen) this.panels[0]?.expand();
  }

  private collapseOthers(panel: RlAccordionPanel) {
    for (const otherPanel of this.panels) {
      if (otherPanel !== panel) {
        otherPanel.collapse();
      }
    }
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'rl-accordion': RlAccordion;
  }
}
