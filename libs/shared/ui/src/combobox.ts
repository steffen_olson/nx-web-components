import {css, html, LitElement} from 'lit';
import {
  customElement,
  queryAssignedElements,
  state,
  property,
} from 'lit/decorators.js';
import {classMap} from 'lit/directives/class-map.js';

@customElement('rl-combobox')
export class RlCombobox extends LitElement {
  static listenerCount = 1;

  static override styles = css`
    :host {
      display: inline-block;
      position: relative;
    }

    :host([hidden]) {
      display: none;
    }

    .combobox-popover {
      display: none;
      position: absolute;
      border: 1px solid lightgray;
      width: 100%;
    }

    .combobox-popover.expanded {
      display: block;
    }
  `;

  @state() expanded = false;

  /**
   * This is most likely not how this should be implemented for real. Instead...
   * a combo box could be more generic. Could slot in any type of element that implements
   * some interface that would deal with the actual selection business. Could be a
   * datepicker or a listbox or whatever else. This element could potentionally be pretty
   * "dumb" in that it just renders an input and some content inside a popover. And the
   * real work could be done in the parent component and the slotted component. The slotted
   * component would emit a selection event, and the parent component would take care to
   * updated the selected value and label.
   *
   * Also, shouldn't use a native `option` because its `value` is a string.
   */
  @queryAssignedElements({selector: 'option'}) options!: HTMLOptionElement[];

  @state() selectedLabel = '';
  @property() selectedValue: any | null = null;

  override render() {
    return html`
      <input
        type="text"
        .value=${this.selectedLabel}
        @input=${this.handleInput}
        @focus=${this.handleInputFocus}
        @blur=${this.handleInputBlur}
      />
      <div
        class=${classMap({'combobox-popover': true, expanded: this.expanded})}
      >
        <slot @slotchange=${this.handleSlotChange}></slot>
      </div>
    `;
  }

  select(option: HTMLOptionElement) {
    this.selectedValue = option.value;
    this.selectedLabel = option.label ?? option.textContent ?? '';
  }

  private handleInput(ev: InputEvent): void {
    this.selectedLabel = (ev.target as HTMLInputElement).value;

    this.dispatchEvent(
      new CustomEvent<string>('filterValueChange', {detail: this.selectedLabel})
    );
  }

  private handleInputFocus(): void {
    this.expanded = true;
  }

  private handleInputBlur(): void {
    // this.expanded = false;
  }

  private handleSlotChange(): void {
    this.resetOptions();
  }

  private resetOptions(): void {
    /**
     * It might make a more sense for the items to fire an event that bubbles
     * and just have a single listener at this level.
     */
    for (const opt of this.options) {
      opt.addEventListener('click', this.optionSelected);
    }
  }

  private optionSelected = (ev: MouseEvent) => {
    const opt = ev.target as HTMLOptionElement;
    console.log(`${opt.value} selected, ${typeof opt.value}`);
    this.select(opt);
    this.expanded = false;
  };
}

declare global {
  interface HTMLElementTagNameMap {
    'rl-combobox': RlCombobox;
  }
}
