import {LitElement, css, html, TemplateResult} from 'lit';
import {customElement, property, query, state} from 'lit/decorators.js';
import {classMap} from 'lit/directives/class-map.js';

@customElement('rl-textfield')
export class RlTextfield extends LitElement {
  static override styles = css`
    :host {
      display: inline-flex;
      position: relative;
      min-width: 256px;
      margin: 8px 8px 0 8px;
    }

    .rl-textfield-outline {
      width: 100%;
      display: block;
      border-radius: 4px;
      border: 2px solid red;
      height: 56px;
      padding: 0 16px;
    }

    .rl-textfield-outline--focused {
      border-color: green;
    }

    .rl-textfield-label {
      transition: transform 150ms ease-in-out, font-size 150ms ease-in-out;
      position: absolute;
      top: 50%;
      transform: translateY(-50%);
      padding: 0 4px;
    }

    .rl-textfield-label--floated {
      font-size: 75%;
      transform: translate(-4px, -38px);
      background: white;
    }

    .rl-textfield-input {
      border: 0;
      background: 0;
      padding: 0;
      height: 100%;
    }

    .rl-textfield-input:focus {
      outline: 0;
    }
  `;

  @query('.rl-textfield-input') inputElement!: HTMLInputElement;
  @query('#label') labelElement!: HTMLElement | null;

  @property() type: 'text' | 'password' | 'email' = 'text';
  @property() value = '';
  @property() label = '';

  @state() focused = false;

  override focus() {
    const ev = new CustomEvent('focus');
    this.dispatchEvent(ev);
    this.inputElement.focus();
  }

  override blur() {
    const ev = new CustomEvent('blur');
    this.dispatchEvent(ev);
    this.inputElement.blur();
  }

  override render() {
    return html`
      <label
        class="${classMap({
          'rl-textfield-outline': true,
          'rl-textfield-outline--focused': this.focused,
        })}"
      >
        ${this.renderLabel()}
        <input
          class="rl-textfield-input"
          type="${this.type}"
          value=${this.value}
          @focus=${this.inputFocused}
          @blur=${this.inputBlurred}
        />
      </label>
    `;
  }

  private renderLabel(): TemplateResult | string {
    if (this.label) {
      const classes = classMap({
        'rl-textfield-label': true,
        'rl-textfield-label--floated': this.focused,
      });
      return html`<span class=${classes} id="label">${this.label}</span>`;
    }

    return '';
  }

  private inputFocused() {
    this.focused = true;
  }

  private inputBlurred() {
    this.focused = false;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'rl-textfield': RlTextfield;
  }
}
