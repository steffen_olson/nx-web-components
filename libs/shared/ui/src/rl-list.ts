import {html, LitElement} from 'lit';
import {customElement, property} from 'lit/decorators.js';

@customElement('rl-list')
export class RlList extends LitElement {
  @property({type: Array}) items!: unknown[];

  override render() {
    if (!this.items) {
      return html`whoopsies`;
    }

    return html`
      <ul>
        ${this.items.map((item) => this.renderItem(item))}
      </ul>
    `;
  }

  private renderItem(item: unknown) {
    return html`<li>${item}</li>`;
  }

  protected override createRenderRoot(): Element | ShadowRoot {
    return this;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'rl-list': RlList;
  }
}
