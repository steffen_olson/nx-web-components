# srl-list



<!-- Auto Generated Below -->


## Properties

| Property    | Attribute | Description | Type        | Default |
| ----------- | --------- | ----------- | ----------- | ------- |
| `listItems` | --        |             | `unknown[]` | `[]`    |
| `open`      | `open`    |             | `boolean`   | `true`  |


## Events

| Event        | Description | Type                   |
| ------------ | ----------- | ---------------------- |
| `openChange` |             | `CustomEvent<boolean>` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
