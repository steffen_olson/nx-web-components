import { Component, Prop, Event, Host, EventEmitter, h } from '@stencil/core';

@Component({
  tag: 'srl-list',
})
export class SrlList {
  @Prop() open = true;
  @Event() openChange: EventEmitter<boolean>;

  @Prop() listItems: unknown[] = [];

  toggleHandler() {
    this.openChange.emit(!this.open);
  }

  render() {
    console.log('rerender', this.listItems);
    return (
      <Host>
        <button onClick={() => this.toggleHandler()}>toggle</button>
        {this.open && (
          <ul>
            {this.listItems.map(item => (
              <li>{item.toString()}</li>
            ))}
          </ul>
        )}
      </Host>
    );
  }
}
