import { Config } from '@stencil/core';
import { angularOutputTarget as angular, ValueAccessorConfig } from '@stencil/angular-output-target';

export const config: Config = {
  namespace: 'stencil-ui',
  outputTargets: [
    angular({
      componentCorePackage: '@wc-nx/shared/stencil-ui',
      directivesProxyFile: '../stencil-ui-ng/src/lib/components.ts',
      includeImportCustomElements: true,
    }),
    {
      type: 'dist',
      esmLoaderPath: '../loader',
    },
    {
      type: 'dist-custom-elements',
    },
    {
      type: 'docs-readme',
    },
    {
      type: 'www',
      serviceWorker: null, // disable service workers
    },
  ],
};
