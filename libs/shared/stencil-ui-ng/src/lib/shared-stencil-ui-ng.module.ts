import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SrlList } from './components';

@NgModule({
  declarations: [SrlList],
  exports: [SrlList],
  imports: [CommonModule],
})
export class SharedStencilUiNgModule {}
