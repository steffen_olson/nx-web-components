/* tslint:disable */
/* auto-generated angular directive proxies */
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  NgZone,
  Output,
} from '@angular/core';
import { ProxyCmp, proxyOutputs } from './angular-component-lib/utils';

import type { Components } from '@wc-nx/shared/stencil-ui/components';

import { defineCustomElement as defineMyComponent } from '@wc-nx/shared/stencil-ui/components/my-component.js';
import { defineCustomElement as defineSrlList } from '@wc-nx/shared/stencil-ui/components/srl-list.js';

export declare interface MyComponent extends Components.MyComponent {}

@ProxyCmp({
  defineCustomElementFn: defineMyComponent,
  inputs: ['first', 'last', 'middle'],
})
@Component({
  selector: 'my-component',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['first', 'last', 'middle'],
})
export class MyComponent {
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
  }
}

export declare interface SrlList extends Components.SrlList {
  /**
   *
   */
  // openChange: EventEmitter<CustomEvent<boolean>>;
}

@ProxyCmp({
  defineCustomElementFn: defineSrlList,
  inputs: ['listItems', 'open'],
})
@Component({
  selector: 'srl-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: '<ng-content></ng-content>',
  inputs: ['listItems', 'open'],
})
export class SrlList {
  @Output() openChange = new EventEmitter<CustomEvent<boolean>>();
  protected el: HTMLElement;
  constructor(c: ChangeDetectorRef, r: ElementRef, protected z: NgZone) {
    c.detach();
    this.el = r.nativeElement;
    // proxyOutputs(this, this.el, ['openChange']);
  }
}
