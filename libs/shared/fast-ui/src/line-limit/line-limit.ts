import {
  FASTElement,
  customElement,
  html,
  css,
  attr,
  observable,
  nullableNumberConverter,
} from '@microsoft/fast-element';

export const lineLimitStyles = css`
  :host {
    display: block;
  }

  :host([hidden]) {
    display: hidden;
  }
`;

export const lineLimitTemplate = html<LineLimit>`
  <template style=${(x) => x.lineClamp}>
    <slot></slot>
  </template>
`;

@customElement({
  name: 'frl-line-limit',
  styles: lineLimitStyles,
  template: lineLimitTemplate,
})
export class LineLimit extends FASTElement {
  /**
   * @internal
   */
  @observable
  lineClamp = '-webkit-line-clamp: 2';

  @attr({ converter: nullableNumberConverter }) limit: number | null = null;

  limitChanged() {
    console.log(typeof this.limit);
    if (!this.limit || this.limit < 1) {
      this.lineClamp = '';
    } else {
      this.lineClamp = `display:-webkit-box;-webkit-box-orient:vertical;overflow:hidden;-webkit-line-clamp:${this.limit};`;
    }
  }
}
