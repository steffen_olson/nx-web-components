import {
  FASTElement,
  customElement,
  attr,
  html,
  css,
} from '@microsoft/fast-element';

// NOTE: can put this in a different file if desired
export const helloTemplate = html<Hello>`<div>Hello, ${(x) => x.name}!</div>`;

export const helloStyles = css`
  :host {
    display: block;
  }

  :host([hidden]) {
    display: none;
  }

  div {
    color: red;
  }
`;

// NOTE: in a real library component, it is desirable to _not_ use this decorator, and instead extend
// FoundationElement and register it with the design system. Alows application authors to modify templates
// or styles as needed and ties it in with the rest of the design system.
@customElement({
  name: 'frl-hello',
  template: helloTemplate,
  styles: helloStyles,
})
export class Hello extends FASTElement {
  @attr name: string = 'World';

  nameChanged() {
    console.log(this.name, 'changed');
  }
}
