import { css } from '@microsoft/fast-element';
import {
  TextField,
  textFieldTemplate as template,
} from '@microsoft/fast-foundation';

const styles = css``;

export const frlTextField = TextField.compose({
  baseName: 'text-field',
  styles,
  template,
  shadowOptions: {
    delegatesFocus: true,
  },
});

export const textFieldStyles = styles;
export const textFieldTemplate = template;
