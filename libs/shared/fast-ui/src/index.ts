import { DesignSystem } from '@microsoft/fast-foundation';

export * from './button';
export * from './dialog';
export * from './hello';
export * from './line-limit';
export * from './text-field';

export function provideFrlDesignSystem(element?: HTMLElement): DesignSystem {
  return DesignSystem.getOrCreate(element).withPrefix('frl');
}
