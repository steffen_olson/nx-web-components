import { css } from '@microsoft/fast-element';
import {
  Dialog as FoundationDialog,
  dialogTemplate as template,
} from '@microsoft/fast-foundation';

const styles = css`
  :host([hidden]) {
    display: none;
  }

  :host {
    display: block;
  }

  .overlay {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background: rgba(0, 0, 0, 0.2);
    touch-action: none;
  }

  .positioning-region {
    display: flex;
    justify-content: center;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    overflow: auto;
  }

  .control {
    margin-top: auto;
    margin-bottom: auto;
    width: 480px;
    height: 320px;
    background-color: white;
    z-index: 1;
    border-radius: 4px;
    border: 2px solid blue;
  }
`;

export class Dialog extends FoundationDialog {}

export const frlDialog = Dialog.compose({
  baseName: 'dialog',
  baseClass: FoundationDialog,
  template,
  styles,
});

export const dialogStyles = styles;
export const dialogTemplate = template;
