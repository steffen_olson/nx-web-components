import { css } from '@microsoft/fast-element';
import { Button, buttonTemplate as template } from '@microsoft/fast-foundation';

export const buttonStyles = css`
  .control {
    padding: 8px;
    border: 2px solid blue;
    border-radius: 4px;
  }
`;

export const frlButton = Button.compose({
  baseName: 'button',
  template,
  styles: buttonStyles,
  shadowOptions: {
    delegatesFocus: true,
  },
});

export const buttonTemplate = template;
